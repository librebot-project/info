# About LibreBot
LibreBot is an open source, AI-powered robot that you, the user, are responsible for teaching. It learns just like a human.
You also build your own LibreBot.
